package com.example.manar.tahrirlounge.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.example.manar.tahrirlounge.R;

public class WebViewActivity extends AppCompatActivity {
    WebView mWebView;
    Boolean flag=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        mWebView =  (WebView) findViewById(R.id.webview_fragment);

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        //  webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.setAllowFileAccess(true);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setNeedInitialFocus(true);
        mWebView.setInitialScale(1);
        mWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);

        // TODO Read This Ya Manar Elly ma ben shorat :V
        //------------------------------------------------------------------------------------------------------
        // This line is to put the view inside the app ... if it's uncommented
        // If it's commented as below the webview will go to see the app relevant or a brower to initiate the url
//        mWebView.setWebViewClient(new WebViewClient());


        // another line would be this one to start chrome
        //        mWebView.setWebChromeClient(new WebChromeClient());
        //------------------------------------------------------------------------------------------------------

        if(getIntent().hasExtra("url"))
            mWebView.loadUrl(getIntent().getStringExtra("url"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(flag)
            finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        flag=true;
    }
}
