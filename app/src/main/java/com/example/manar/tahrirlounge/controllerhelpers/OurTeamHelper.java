package com.example.manar.tahrirlounge.controllerhelpers;

import android.app.Activity;
import android.view.View;
import android.widget.Toast;

import com.example.manar.tahrirlounge.controller.OurTeamController;
import com.example.manar.tahrirlounge.models.TeamMembers;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.manar.tahrirlounge.view.MainActivity.progressBar;

/**
 * Created by manar on 30/08/17.
 */

public class OurTeamHelper {

    OurTeamInterface  ourTeamInterface;
    OurTeamController ourTeamController;
    Activity context;

    public OurTeamHelper(OurTeamController ourTeamController, Activity context) {
        this.ourTeamController = ourTeamController;
        this.context = context;
    }

    public void setOnEventListener (OurTeamInterface l){
        this.ourTeamInterface = l;
    }

    public void getOurTeamMembers() {


        ourTeamController.getAllTeamMembers(new Callback<TeamMembers>() {
            @Override
            public void onResponse(Call<TeamMembers> call, Response<TeamMembers> response) {
                if(response.code()>=200&&response.code()<400)
                ourTeamInterface.getOurTeamMembers(response.body());
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<TeamMembers> call, Throwable t) {
                Toast.makeText(context, "Failed To Fetch Data", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

}
