package com.example.manar.tahrirlounge.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.manar.tahrirlounge.R;
import com.example.manar.tahrirlounge.adapters.OurTeamAdapter;
import com.example.manar.tahrirlounge.controller.OurTeamController;
import com.example.manar.tahrirlounge.controllerhelpers.OurTeamHelper;
import com.example.manar.tahrirlounge.controllerhelpers.OurTeamInterface;
import com.example.manar.tahrirlounge.models.TeamMembers;

import java.util.ArrayList;

import static com.example.manar.tahrirlounge.view.MainActivity.progressBar;

public class OurTeamFragment extends Fragment {
    OurTeamHelper ourTeamHelper;
    OurTeamController ourTeamController;


    public OurTeamFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    RecyclerView our_team_rv;
    TeamMembers teamMembers;

    ArrayList<TeamMembers.TeamMembersModel> teamMembersModelAL;
    OurTeamAdapter ourTeamAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_our_team, container, false);
        our_team_rv =  view.findViewById(R.id.our_team_rv);
        our_team_rv.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        our_team_rv.setLayoutManager(linearLayoutManager);

        teamMembers = new TeamMembers();
        ourTeamController = new OurTeamController();

        progressBar.setVisibility(View.VISIBLE);

        getOurTeams();
//        com.example.manar.tahrirlounge.Rest.NetworkService.setBaseUrl("http://209.126.105.42:8001");

        return view ;
    }

    private void getOurTeams() {
        ourTeamHelper = new OurTeamHelper(ourTeamController,getActivity());
        ourTeamHelper.setOnEventListener(new OurTeamInterface() {
            @Override
            public void getOurTeamMembers(TeamMembers tm1) {
                teamMembers = tm1;


              /*  teamMembersModelAL = new ArrayList<TeamMembers.TeamMembersModel>();
                for(TeamMembers.TeamMembersModel teamMembersModel:teamMembers.getTeamMembersModel()){
                    teamMembersModelAL.add(teamMembersModel);
                }*/
              Log.v("HelloItHelloIt'sMe","InInterface");
                ourTeamAdapter = new OurTeamAdapter(teamMembers,getActivity());
                our_team_rv.setAdapter(ourTeamAdapter);
            }
        });
        ourTeamHelper.getOurTeamMembers();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }


//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }
}
