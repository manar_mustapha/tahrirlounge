package com.example.manar.tahrirlounge.controller;

import com.example.manar.tahrirlounge.models.TeamMembers;
import com.example.manar.tahrirlounge.rest.NetworkAPI;
import com.example.manar.tahrirlounge.rest.NetworkService;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by hossam on 30/08/17.
 */

public class OurTeamController {
    public OurTeamController() {
    }

    public void getAllTeamMembers(Callback<TeamMembers> callback) {
        NetworkService networkService = new NetworkService("http://209.126.105.42:8001/iosapi/");
        NetworkAPI networkAPI = networkService.getAPI();
        Call<TeamMembers> call = networkAPI.getAllTeamMembers();
        call.enqueue(callback);
    }

}
