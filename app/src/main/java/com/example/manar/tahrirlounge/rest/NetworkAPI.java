package com.example.manar.tahrirlounge.rest;

import com.example.manar.tahrirlounge.models.EventsModel;
import com.example.manar.tahrirlounge.models.GalleryModel;
import com.example.manar.tahrirlounge.models.TeamMembers;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by manar on 27/08/17.
 */

public interface NetworkAPI {

    @GET("events")
    Call<List<EventsModel>> getEventsList();

    @GET("getAllTeamMembers")
    Call<TeamMembers> getAllTeamMembers();

    @GET("workshops")
    Call<List<GalleryModel>> getGalleryList();

}
