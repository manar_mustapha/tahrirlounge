package com.example.manar.tahrirlounge.controllerhelpers;

import com.example.manar.tahrirlounge.models.EventsModel;

import java.util.List;

/**
 * Created by manar on 30/08/17.
 */

public interface EventsInterface {

    void getEvents(List<EventsModel> eventsModelList);
}
