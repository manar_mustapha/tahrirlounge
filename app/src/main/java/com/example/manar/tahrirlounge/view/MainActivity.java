package com.example.manar.tahrirlounge.view;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.manar.tahrirlounge.R;
import com.example.manar.tahrirlounge.adapters.CustomDrawerAdapter;
import com.example.manar.tahrirlounge.adapters.DrawerItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    CustomDrawerAdapter adapter;
    public static ProgressBar progressBar;
    Toolbar toolbar;
    List<DrawerItem> dataList;
    ImageView imageview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Initializing
        progressBar = (ProgressBar) findViewById(R.id.progress_bar) ;
        dataList = new ArrayList<>();
        mTitle = mDrawerTitle = getTitle();
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout.setDrawerShadow(R.drawable.upload, GravityCompat.START);

        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.nav_header
                , null, false);

        mDrawerList.addHeaderView(listHeaderView);
        // Add Drawer Item to dataList
        dataList.add(new DrawerItem("Home", R.drawable.news_icon));
        dataList.add(new DrawerItem("About Us",  R.drawable.info_icon));
        dataList.add(new DrawerItem("Events", R.drawable.events_icon));
        dataList.add(new DrawerItem("Gallery",  R.drawable.gallery_icon));
        dataList.add(new DrawerItem("Contact Us", R.drawable.contact_us_icon));
        dataList.add(new DrawerItem("Facebook Page",  R.drawable.facebook_icon));
        dataList.add(new DrawerItem("Twitter",  R.drawable.twitter_icon));
        dataList.add(new DrawerItem("Youtube",  R.drawable.youtube_icon));
        dataList.add(new DrawerItem("Our Team", R.drawable.team_icon));
        dataList.add(new DrawerItem("Our Partners",  R.drawable.news_icon));
        dataList.add(new DrawerItem("About App", R.drawable.news_icon));

        adapter = new CustomDrawerAdapter(this, R.layout.custom_drawer_item, dataList);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                toolbar,
                R.string.drawer_open,
                R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                toolbar.setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to
                // onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                toolbar.setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to
                // onPrepareOptionsMenu()
            }
        };

        if (savedInstanceState == null) {
            SelectItem(0);
        }

//        Drawable drawable = ResourcesCompat.getDrawable(getResources(),   R.drawable.upload, this.getTheme());

//        mDrawerToggle.setHomeAsUpIndicator(drawable);
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });
        mDrawerToggle.setHomeAsUpIndicator(R.drawable.rsz_3rsz_white_drawer);

        mDrawerLayout.setDrawerListener(mDrawerToggle);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void SelectItem(int position) {
        Log.v("HelloItHelloIt'sMe","InInterface -----> " +position);
        switch (position) {
            case 0:
//                fragment = new FragmentOne();
//                args.putString(FragmentOne.ITEM_NAME, dataList.get(possition)
//                        .getItemName());
//                args.putInt(FragmentOne.IMAGE_RESOURCE_ID, dataList.get(possition)
//                        .getImgResID());

            break;
            case 1:

                break;
            case 2: {
                transactFragment(new AboutUsFragment());
                break;
            }
            case 3: {
                transactFragment(new EventsFragment());
                break;
            }
            case 4: {
                transactFragment(new GalleryFragment());
                break;
            }
            case 5: {
                transactFragment(new ContactUsFragment());
//                startActivity(new Intent(MainActivity.this, WebViewActivity.class)
//                        .putExtra("url", "https://www.facebook.com/TahrirLounge/"));
                break;
            }
            case 6: {
//                Fragment youtube_webViewFragment = WebViewFragment.newInstance("https://www.youtube.com/user/Tahrirlounge");
//                transactFragment(youtube_webViewFragment);

                //TODO Ana Grbt Both Activity w Fragment , uncomment w shofe both
                //TODO Note : El Activity feha 2 options check Them Out
                Fragment facebook_WebViewFragment = WebViewFragment.newInstance("https://www.facebook.com/TahrirLounge/");
                transactFragment(facebook_WebViewFragment);
                break;
            }
            case 7: {
                startActivity(new Intent(MainActivity.this, WebViewActivity.class)
                        .putExtra("url", "https://twitter.com/Tahrirlounge"));
                break;
            }
            case 8: {
                startActivity(new Intent(MainActivity.this, WebViewActivity.class)
                        .putExtra("url", "https://www.youtube.com/user/Tahrirlounge"));
                break;
            }
            case 9: {
                transactFragment(new OurTeamFragment());
                break;
            }
            case 10: {
                transactFragment(new OurPartnerFragment());
                break;
            }
            case 11: {
                transactFragment(new AboutUsFragment());
                break;
            }

        }

//        fragment.setArguments(args);
//        FragmentManager frgManager = getFragmentManager();
//        frgManager.beginTransaction().replace(R.id.content_frame, fragment)
//                .commit();

        mDrawerList.setItemChecked(position, true);
        if(position>0)
        setTitle(dataList.get(position-1).getItemName());
        else
            setTitle(dataList.get(position).getItemName());
        mDrawerLayout.closeDrawer(mDrawerList);

    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        toolbar.setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            SelectItem(position);

        }
    }


    public void transactFragment(Fragment fragment) {
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}