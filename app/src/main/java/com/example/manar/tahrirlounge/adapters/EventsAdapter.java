package com.example.manar.tahrirlounge.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.manar.tahrirlounge.R;
import com.example.manar.tahrirlounge.models.EventsModel;

import java.util.List;

/**
 * Created by hossam on 28/08/17.
 */

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventViewHolder>{

    EventsAdapter.OnClickHandler onClickHandler;
    List<EventsModel> eventsModel ;
    Context mContext;

    public EventsAdapter(Context mContext , List<EventsModel> eventsModel, EventsAdapter.OnClickHandler onClickHandler){

        this.onClickHandler = onClickHandler ;
        this.eventsModel = eventsModel ;
        this.mContext = mContext;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_list_item, parent, false);
        return new EventViewHolder(view);
    }

    public interface OnClickHandler {
        void onSeeMoreClickListener(EventsModel eventsModel);
    }
    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {

        Glide.with(mContext).load(eventsModel.get(position).getEventImage()).into(holder.eventImage);
        holder.eventInstructor.setText(eventsModel.get(position).getEventInstractor());
        holder.eventInstructor.setText(eventsModel.get(position).getEventInstractor());
        holder.eventDetails.setText(eventsModel.get(position).getEventDetails());
        holder.eventName.setText(eventsModel.get(position).getEventName());
        holder.eventDate.setText(eventsModel.get(position).getEventDate());

    }

    @Override
    public int getItemCount() {
        return eventsModel.size();
    }

    public class EventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView eventInstructor ;
        TextView eventDetails ;
        ImageView eventImage ;
        TextView eventName ;
        TextView eventDate ;
        Button seeMore ;

        public EventViewHolder(View itemView) {
            super(itemView);

            eventInstructor = itemView.findViewById(R.id.event_instructor);
            eventDetails = itemView.findViewById(R.id.event_details);
            eventImage = itemView.findViewById(R.id.event_image);
            eventDate = itemView.findViewById(R.id.event_date);
            eventName = itemView.findViewById(R.id.event_name);
            seeMore = itemView.findViewById(R.id.see_more);
            seeMore.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int p = getAdapterPosition();
            onClickHandler.onSeeMoreClickListener(eventsModel.get(p));
        }
    }
}
