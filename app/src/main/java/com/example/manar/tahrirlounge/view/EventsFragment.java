package com.example.manar.tahrirlounge.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.manar.tahrirlounge.R;
import com.example.manar.tahrirlounge.adapters.EventsAdapter;
import com.example.manar.tahrirlounge.controller.EventsController;
import com.example.manar.tahrirlounge.controllerhelpers.EventsHelper;
import com.example.manar.tahrirlounge.controllerhelpers.EventsInterface;
import com.example.manar.tahrirlounge.models.EventsModel;

import java.util.List;

import static com.example.manar.tahrirlounge.view.MainActivity.progressBar;

public class EventsFragment extends Fragment implements EventsInterface{

    public EventsFragment() {
        // Required empty public constructor
    }

    EventsController eventsController ;
    RecyclerView eventsRecycleView;
    EventsAdapter eventsAdapter ;
    EventsHelper eventsHelper ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_events, container, false);
        eventsRecycleView = view.findViewById(R.id.events_recycle_view);
        eventsRecycleView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        eventsRecycleView.setLayoutManager(linearLayoutManager);
        eventsController = new EventsController() ;
        eventsHelper = new EventsHelper(eventsController);
        eventsHelper.setEventsInterface(this);
        eventsHelper.getEventsHelper();
        progressBar.setVisibility(View.VISIBLE);
        return view ;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void getEvents(List<EventsModel> eventsModelList) {
        eventsAdapter = new EventsAdapter(getActivity(), eventsModelList, new EventsAdapter.OnClickHandler() {
            @Override
            public void onSeeMoreClickListener(EventsModel eventsModel) {

                android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment newFragment = new EventsDetailsFragment();
                Bundle mBundle = new Bundle();
                mBundle.putParcelable("eventModel", eventsModel);
                newFragment.setArguments(mBundle);
                fragmentTransaction.replace(R.id.fragment, newFragment);
                fragmentTransaction.addToBackStack("");
                fragmentTransaction.commit();
            }
        });
        eventsRecycleView.setAdapter(eventsAdapter);
    }
}
