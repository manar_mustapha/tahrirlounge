package com.example.manar.tahrirlounge.controllerhelpers;

import com.example.manar.tahrirlounge.models.GalleryModel;

import java.util.List;

/**
 * Created by manar on 09/09/17.
 */

public interface GalleryInterface {

    void onGalleryListFetched(List<GalleryModel> galleryList);

}
