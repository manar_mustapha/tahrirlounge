package com.example.manar.tahrirlounge.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.manar.tahrirlounge.R;
import com.example.manar.tahrirlounge.adapters.GalleryAdapter;
import com.example.manar.tahrirlounge.adapters.GalleryStudioAdapter;
import com.example.manar.tahrirlounge.controller.GalleryController;
import com.example.manar.tahrirlounge.controllerhelpers.GalleryHelper;
import com.example.manar.tahrirlounge.models.GalleryModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryStudioFragment extends Fragment implements View.OnClickListener{

    GalleryStudioAdapter galleryStudioAdapter;
    RecyclerView galleryRecycleView;
    ImageView gallery_image_view ;
    GalleryModel galleryModel ;
    TextView galleryTitle ;
    Bundle mBundle ;

    public GalleryStudioFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_gallery_studio, container, false);

        if (getArguments() != null) {
            mBundle = getArguments() ;
            galleryModel = mBundle.getParcelable("galleryModel");
        }
        galleryRecycleView = view.findViewById(R.id.gallery_recycle_view);
        gallery_image_view = view.findViewById(R.id.gallery_image_view);
        galleryTitle = view.findViewById(R.id.gallery_title);
        galleryRecycleView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL , false);
        galleryRecycleView.setLayoutManager(linearLayoutManager);
        galleryRecycleView.setVisibility(View.VISIBLE);

        return view ;
    }

    @Override
    public void onStart() {
        super.onStart();
        galleryTitle.setText(galleryModel.getName());
        galleryStudioAdapter = new GalleryStudioAdapter(galleryModel.getGallery(), getActivity(), new GalleryStudioAdapter.OnClickHandler() {
            @Override
            public void onImageClick(String galleryImage) {
                Glide.with(getActivity()).load(galleryImage).into(gallery_image_view);

            }
        });
        galleryRecycleView.setAdapter(galleryStudioAdapter);
        gallery_image_view.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if(id == R.id.gallery_image_view){
            if(galleryRecycleView.getVisibility() == view.VISIBLE){
                galleryRecycleView.setVisibility(View.GONE);
            }else if (galleryRecycleView.getVisibility() == view.GONE){
                galleryRecycleView.setVisibility(View.VISIBLE);
            }
        }
    }
}
