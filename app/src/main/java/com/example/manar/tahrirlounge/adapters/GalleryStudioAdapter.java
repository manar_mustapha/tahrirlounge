package com.example.manar.tahrirlounge.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.manar.tahrirlounge.R;
import com.example.manar.tahrirlounge.models.GalleryModel;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by hossam on 28/08/17.
 */

public class GalleryStudioAdapter extends RecyclerView.Adapter<GalleryStudioAdapter.GalleryViewHolder> {

    GalleryStudioAdapter.OnClickHandler onClickHandler;
    List<String> galleryImages;
    Context context;

    public GalleryStudioAdapter(List<String> galleryImages, Context context , GalleryStudioAdapter.OnClickHandler onClickHandler) {

        this.onClickHandler = onClickHandler ;
        this.galleryImages = galleryImages;
        this.context = context;
    }

    public interface OnClickHandler {
        void onImageClick(String galleryImage);
    }
    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate
                (R.layout.gallery_list_item, parent, false);
        return new GalleryViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(GalleryViewHolder holder, int position) {

        Glide.with(context).load(galleryImages.get(position)).into(holder.gallery_image_view);

    }

    @Override
    public int getItemCount() {
        return galleryImages.size();
    }

    public class GalleryViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{

        CircleImageView gallery_image_view;

        public GalleryViewHolder(View itemView) {
            super(itemView);
            gallery_image_view = itemView.findViewById(R.id.gallery_image_view);
            gallery_image_view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int p = getAdapterPosition();
            onClickHandler.onImageClick(galleryImages.get(p));
        }
    }
}
