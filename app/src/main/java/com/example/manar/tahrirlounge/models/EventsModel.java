package com.example.manar.tahrirlounge.models;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventsModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("eventName")
    @Expose
    private String eventName;
    @SerializedName("eventImage")
    @Expose
    private String eventImage;
    @SerializedName("eventInstractor")
    @Expose
    private String eventInstractor;
    @SerializedName("eventDetails")
    @Expose
    private String eventDetails;
    @SerializedName("eventDate")
    @Expose
    private String eventDate;
    @SerializedName("instractorImage")
    @Expose
    private String instractorImage;
    public final static Parcelable.Creator<EventsModel> CREATOR = new Creator<EventsModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public EventsModel createFromParcel(Parcel in) {
            EventsModel instance = new EventsModel();
            instance.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.eventName = ((String) in.readValue((String.class.getClassLoader())));
            instance.eventImage = ((String) in.readValue((String.class.getClassLoader())));
            instance.eventInstractor = ((String) in.readValue((String.class.getClassLoader())));
            instance.eventDetails = ((String) in.readValue((String.class.getClassLoader())));
            instance.eventDate = ((String) in.readValue((String.class.getClassLoader())));
            instance.instractorImage = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public EventsModel[] newArray(int size) {
            return (new EventsModel[size]);
        }

    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventImage() {
        return eventImage;
    }

    public void setEventImage(String eventImage) {
        this.eventImage = eventImage;
    }

    public String getEventInstractor() {
        return eventInstractor;
    }

    public void setEventInstractor(String eventInstractor) {
        this.eventInstractor = eventInstractor;
    }

    public String getEventDetails() {
        return eventDetails;
    }

    public void setEventDetails(String eventDetails) {
        this.eventDetails = eventDetails;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getInstractorImage() {
        return instractorImage;
    }

    public void setInstractorImage(String instractorImage) {
        this.instractorImage = instractorImage;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(eventName);
        dest.writeValue(eventImage);
        dest.writeValue(eventInstractor);
        dest.writeValue(eventDetails);
        dest.writeValue(eventDate);
        dest.writeValue(instractorImage);
    }

    public int describeContents() {
        return 0;
    }

}
