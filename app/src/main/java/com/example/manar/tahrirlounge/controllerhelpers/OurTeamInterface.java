package com.example.manar.tahrirlounge.controllerhelpers;

import com.example.manar.tahrirlounge.models.TeamMembers;

/**
 * Created by manar on 30/08/17.
 */

public interface OurTeamInterface {
    void getOurTeamMembers(TeamMembers teamMembers);
}
