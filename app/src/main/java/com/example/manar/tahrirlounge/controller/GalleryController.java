package com.example.manar.tahrirlounge.controller;

import com.example.manar.tahrirlounge.models.EventsModel;
import com.example.manar.tahrirlounge.models.GalleryModel;
import com.example.manar.tahrirlounge.rest.NetworkAPI;
import com.example.manar.tahrirlounge.rest.NetworkService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by manar on 09/09/17.
 */

public class GalleryController {


    public GalleryController() {

    }

    public void getEvents (Callback<List<GalleryModel>> callback){

        NetworkService networkService = new NetworkService("http://tahrirlounge.net/event/api/");
        NetworkAPI networkAPI = networkService.getAPI();
        Call<List<GalleryModel>> call = networkAPI.getGalleryList();
        call.enqueue(callback);

    }
}
