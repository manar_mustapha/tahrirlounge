package com.example.manar.tahrirlounge.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.manar.tahrirlounge.R;
import com.example.manar.tahrirlounge.adapters.GalleryAdapter;
import com.example.manar.tahrirlounge.controller.GalleryController;
import com.example.manar.tahrirlounge.controllerhelpers.GalleryHelper;
import com.example.manar.tahrirlounge.controllerhelpers.GalleryInterface;
import com.example.manar.tahrirlounge.models.GalleryModel;

import java.util.List;

import static com.example.manar.tahrirlounge.view.MainActivity.progressBar;

public class GalleryFragment extends Fragment implements GalleryInterface{

    GalleryController galleryController;
    RecyclerView galleryRecycleView;
    GalleryAdapter galleryAdapter;
    GalleryHelper galleryHelper;

    public GalleryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_gallery, container, false);
        galleryRecycleView = view.findViewById(R.id.gallery_recycle_view);
        galleryRecycleView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        galleryRecycleView.setLayoutManager(gridLayoutManager);
        galleryController = new GalleryController();
        galleryHelper = new GalleryHelper(galleryController , getActivity());
        galleryHelper.setGalleryInterface(this);
        galleryHelper.getGalleryHelper();
        progressBar.setVisibility(View.VISIBLE);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onGalleryListFetched(List<GalleryModel> galleryList) {

        galleryAdapter = new GalleryAdapter(galleryList, getActivity(), new GalleryAdapter.OnClickHandler() {
            @Override
            public void onImageClick(GalleryModel galleryModel) {
                android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment newFragment = new GalleryStudioFragment();
                Bundle mBundle = new Bundle();
                mBundle.putParcelable("galleryModel", galleryModel);
                newFragment.setArguments(mBundle);
                fragmentTransaction.replace(R.id.fragment, newFragment);
                fragmentTransaction.addToBackStack("");
                fragmentTransaction.commit();
            }
        });
        galleryRecycleView.setAdapter(galleryAdapter);
    }
}
