package com.example.manar.tahrirlounge.controller;

import com.example.manar.tahrirlounge.models.EventsModel;
import com.example.manar.tahrirlounge.rest.NetworkAPI;
import com.example.manar.tahrirlounge.rest.NetworkService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by manar on 30/08/17.
 */

public class EventsController {

    public EventsController(){

    }

    public void getEvents (Callback<List<EventsModel>> callback){

        NetworkService networkService = new NetworkService("http://tahrirlounge.net/event/api/");
        NetworkAPI networkAPI = networkService.getAPI();
        Call<List<EventsModel>> call = networkAPI.getEventsList();
        call.enqueue(callback);

    }
}
