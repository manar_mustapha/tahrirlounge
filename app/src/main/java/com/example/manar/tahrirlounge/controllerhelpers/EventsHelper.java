package com.example.manar.tahrirlounge.controllerhelpers;

import android.view.View;

import com.example.manar.tahrirlounge.controller.EventsController;
import com.example.manar.tahrirlounge.models.EventsModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.manar.tahrirlounge.view.MainActivity.progressBar;

/**
 * Created by manar on 30/08/17.
 */

public class EventsHelper {

    EventsController eventsController ;
    EventsInterface eventsInterface;

    public EventsHelper(EventsController eventsController) {
        this.eventsController = eventsController ;
    }

    public void setEventsInterface(EventsInterface eventsInterface) {
        this.eventsInterface = eventsInterface;
    }

    public void getEventsHelper() {

        eventsController.getEvents(new Callback<List<EventsModel>>() {
            @Override
            public void onResponse(Call<List<EventsModel>> call, Response<List<EventsModel>> response) {
                if (response != null) {
                    if (response.body() != null)
                        eventsInterface.getEvents(response.body());
                }
                progressBar.setVisibility(View.GONE);
            }
            @Override
            public void onFailure(Call<List<EventsModel>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);

            }
        });
        }
}
