package com.example.manar.tahrirlounge.adapters;

/**
 * Created by manar on 29/08/17.
 */

public class DrawerItem {

    String ItemName;
    int imgResID;

    public DrawerItem(String itemName, int imgResID) {
        super();
        this.imgResID = imgResID;
        ItemName = itemName;
    }

    public void setImgResID(int imgResID) {
        this.imgResID = imgResID;
    }
    public void setItemName(String itemName) {
        ItemName = itemName;
    }
    public String getItemName() {
        return ItemName;
    }
    public int getImgResID() {
        return imgResID;
    }

}