package com.example.manar.tahrirlounge.controllerhelpers;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.example.manar.tahrirlounge.controller.GalleryController;
import com.example.manar.tahrirlounge.models.GalleryModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.manar.tahrirlounge.view.MainActivity.progressBar;

/**
 * Created by manar on 09/09/17.
 */

public class GalleryHelper {

    GalleryController galleryController;
    GalleryInterface galleryInterface;
    Context mContext;

    public GalleryHelper(GalleryController galleryController, Context mContext) {
        this.galleryController = galleryController;
        this.mContext = mContext;
    }

    public void setGalleryInterface(GalleryInterface galleryInterface) {
        this.galleryInterface = galleryInterface;
    }

    public void getGalleryHelper() {

        galleryController.getEvents(new Callback<List<GalleryModel>>() {
            @Override
            public void onResponse(Call<List<GalleryModel>> call, Response<List<GalleryModel>> response) {
                if (response != null) {
                    if (response.body() != null)
                        galleryInterface.onGalleryListFetched(response.body());
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<List<GalleryModel>> call, Throwable t) {

                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
