package com.example.manar.tahrirlounge.view;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.manar.tahrirlounge.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import static com.example.manar.tahrirlounge.view.MainActivity.progressBar;

public class ContactUsFragment extends Fragment implements View.OnClickListener {

    TextView email_tv, website_text_view, num_tv, location_tv;
    com.google.android.gms.maps.MapView map_branch;

    public ContactUsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        website_text_view = view.findViewById(R.id.website_text_view);
        location_tv = view.findViewById(R.id.location_tv);
        map_branch = view.findViewById(R.id.map_branch);
        email_tv = view.findViewById(R.id.email_tv);
        num_tv = view.findViewById(R.id.num_tv);
        website_text_view.setPaintFlags(email_tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        location_tv.setPaintFlags(email_tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        email_tv.setPaintFlags(email_tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        num_tv.setPaintFlags(email_tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        progressBar.setVisibility(View.VISIBLE);
        map_branch.onCreate(null);
        map_branch.onResume();
        map_branch.getMapAsync(
                new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googlemap) {
                        GoogleMap map = googlemap;
                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                        MapsInitializer.initialize(getActivity().getApplicationContext());
                        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 50);
                        } else {
                            double latitude = 30.0457641;
                            double longitude = 31.1660815;
                            LatLng location = new LatLng(latitude, longitude);
                            String snippetTitle = String.valueOf(longitude);
                            String markerTitle = String.valueOf(latitude);
                            map.addMarker(new MarkerOptions()
                                    .title(markerTitle)
                                    .snippet(snippetTitle)
                                    .position(location).icon(BitmapDescriptorFactory.defaultMarker()));
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 17));
//                            relativeLayout.setVisibility(View.GONE);
//                            progressBar.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }
        );
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.num_tv: {
                callTahrir();
                break;
            }
            case R.id.email_tv: {
                break;
            }
            case R.id.website_text_view: {
                goToTahrirWebsite();
                break;
            }
            case R.id.location_tv: {
                break;
            }
        }
    }

    private void goToTahrirWebsite() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 51);
        } else {
            progressBar.setVisibility(View.GONE);
            String url = website_text_view.getText().toString();
            if (!url.startsWith("http://") && !url.startsWith("https://"))
                url = "http://" + url;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 50 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            callTahrir();
        } else if (requestCode == 50)
            Toast.makeText(getActivity(), "Call Permission Not Allowed", Toast.LENGTH_SHORT).show();
        else if (requestCode == 51 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            goToTahrirWebsite();
        } else if (requestCode == 51) {
            Toast.makeText(getActivity(), "Internet Permission Not Allowed", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
        }
    }

    private void callTahrir() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 50);
        } else {
            progressBar.setVisibility(View.GONE);
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + num_tv.getText().toString().trim()));
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getActivity().startActivity(callIntent);
        }
    }
}