package com.example.manar.tahrirlounge.models;

/**
 * Created by manar on 09/09/17.
 */

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GalleryModel implements Parcelable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("gallery")
    @Expose
    private List<String> gallery = null;
    public final static Parcelable.Creator<GalleryModel> CREATOR = new Creator<GalleryModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public GalleryModel createFromParcel(Parcel in) {
            GalleryModel instance = new GalleryModel();
            instance.name = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.gallery, (java.lang.String.class.getClassLoader()));
            return instance;
        }

        public GalleryModel[] newArray(int size) {
            return (new GalleryModel[size]);
        }

    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getGallery() {
        return gallery;
    }

    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
        dest.writeList(gallery);
    }

    public int describeContents() {
        return 0;
    }

}

