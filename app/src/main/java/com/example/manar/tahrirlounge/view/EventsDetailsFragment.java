package com.example.manar.tahrirlounge.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.manar.tahrirlounge.R;
import com.example.manar.tahrirlounge.models.EventsModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventsDetailsFragment extends Fragment {

    TextView eventInstructor ;
    EventsModel eventsModel ;
    TextView eventDetails ;
    ImageView eventImage ;
    TextView eventName ;
    TextView eventDate ;
    Bundle mBundle ;

    public EventsDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_events_details, container, false);
        if (getArguments() != null) {
            mBundle = getArguments() ;
            eventsModel = mBundle.getParcelable("eventModel");
        }
        eventInstructor = view.findViewById(R.id.event_instructor);
        eventDetails = view.findViewById(R.id.event_details);
        eventImage = view.findViewById(R.id.event_image);
        eventDate = view.findViewById(R.id.event_date);
        eventName = view.findViewById(R.id.event_name);

        return  view ;
    }

    @Override
    public void onStart() {
        super.onStart();

        Glide.with(getActivity()).load(eventsModel.getEventImage()).into(eventImage);
        eventInstructor.setText(eventsModel.getEventInstractor());
        eventDetails.setText(eventsModel.getEventDetails());
        eventName.setText(eventsModel.getEventName());
        eventDate.setText(eventsModel.getEventDate());

    }
}
