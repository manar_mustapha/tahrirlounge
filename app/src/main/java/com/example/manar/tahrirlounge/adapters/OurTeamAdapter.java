package com.example.manar.tahrirlounge.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.manar.tahrirlounge.R;
import com.example.manar.tahrirlounge.models.TeamMembers;

/**
 * Created by hossam on 28/08/17.
 */

public class OurTeamAdapter extends RecyclerView.Adapter<OurTeamAdapter.OurTeamViewHolder> {
    TeamMembers teamMembers;
    Activity context;

    public OurTeamAdapter(TeamMembers teamMembers, Activity context) {
        this.teamMembers = teamMembers;
        this.context = context;
    }

    @Override
    public OurTeamViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate
                (R.layout.our_team_list_item, parent, false);
        return new OurTeamAdapter.OurTeamViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(OurTeamViewHolder holder, int position) {

        TeamMembers.TeamMembersModel teamMembersModel = teamMembers.getTeamMembersModel().get(position);
        holder.team_member_name.setText(teamMembersModel.getName());
        holder.team_member_position.setText(teamMembersModel.getPosition());
        Log.v("HelloItHelloIt'sMe",teamMembersModel.getImage()+"teamMembersModel.getImage()");
        Glide.with(context).load("http://"+teamMembersModel.getImage()).into(holder.team_member_iv);


    }

    @Override
    public int getItemCount() {
        return teamMembers.getTeamMembersModel().size();
    }

    public class OurTeamViewHolder extends RecyclerView.ViewHolder {

        TextView team_member_position, team_member_name;
        ImageView team_member_iv;

        public OurTeamViewHolder(View itemView) {
            super(itemView);

            team_member_iv = itemView.findViewById(R.id.team_member_iv);
            team_member_name = itemView.findViewById(R.id.team_member_name);
            team_member_position = itemView.findViewById(R.id.team_member_position);
        }
    }
}
